const programs = [
	{
		label: "Dashboard",
		command: "code ~/projects/electron-dashboard"
	},
	{
		label: "Project Theme",
		command: "code ~/projects/project-theme"
	},
	{
		label: "Go Projects",
		command: "code ~/go"
	},
	{
		label: "Dashboard",
		command: "code ~/projects/electron-dashboard"
	},
	{
		label: "Notes",
		command: "code ~/vscode-notes"
	}
];

export default programs;