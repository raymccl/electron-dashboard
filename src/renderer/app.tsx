import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from "@material-ui/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { exec } from 'child_process';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import programs from '../../config';

export default function App () {
	const muiTheme = createMuiTheme({
		palette: {
			type: 'dark'
		}
	});
	
	console.log(muiTheme)
	
	const useStyles = makeStyles((theme: Theme) =>
		createStyles({
			root: {
			flexGrow: 1,
			},
			button: {
				padding: theme.spacing(1),
				textAlign: 'center',
			},
		}),
	);
	
	const classes = useStyles(muiTheme);

	return (
		<ThemeProvider theme={muiTheme}>
			<Grid
				container
				direction="column"
			>
				{programs.map((program) => (
					<Grid className={classes.button} item xs={12}>
						<Button
							
							variant="contained"
							color='primary'
							onClick={() => {
								exec(program.command);
							}}
						>{program.label}</Button>
					</Grid>
				))}
			</Grid>
		</ThemeProvider>
	);
}